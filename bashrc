# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Auto "cd" and Line wrap on window resize
shopt -s autocd checkwinsize

# Help people new to Arch
alias apt='man pacman'
alias apt-get='man pacman'
alias tb='nc termbin.com 9999'
alias fastest-mirrors='refresh-mirrors'

# Better alternatives for some commands
alias c='clear'
alias sudo='sudo '
alias doas='doas '
alias jctl="journalctl -p 3 -xb"
alias fixpacman="rm /var/lib/pacman/db.lck"
alias mkdir='mkdir -p'
alias open='xdg-open'

# Visual Studio Code Wayland
alias vsc='code --ozone-platform-hint=auto --enable-features=WaylandWindowDecorations --disable-gpu'

# Directory navigation aliases (five levels)
for i in {.,..,...,....,.....}; do
  alias "$i"="cd $i"
  alias "cd$i"="cd $i"
done

# ls and helpers with eza if available
if command -v eza &>/dev/null; then
  alias ls='eza -1 --icons=auto'
  alias l='eza -lh --icons=auto'
  alias ll='eza -lha --icons=auto --sort=name --group-directories-first'
  alias ld='eza -lhD --icons=auto'
fi

# Use 'bat' as replacement for 'cat' and improved help
if command -v bat &>/dev/null; then
  cat() {
    for arg in "$@"; do
      if [[ "$arg" =~ ^-[vet]+$ ]]; then
        command cat "$@"
        return
      fi
    done
    if echo "$1" | grep -iq '\.mdx\?$'; then
      glow "$@"
    else
      bat --style=header --style=snip --style=changes "$@"
    fi
  }

  help() {
    if [ $# -eq 0 ]; then
      command help
    else
      "$@" --help 2>&1 | bat --paging=never --style=plain --language=help
    fi
  }

	# Configure bat for man pages
	export MANPAGER="sh -c 'col -bx | bat -l man -p'"
	export MANROFFOPT="-c"
fi

# Bash completion
[ -x /usr/bin/doas ] && complete -F _command doas

# Dircolors
[ -x /usr/bin/dircolors ] && alias dir='dir --color=auto' grep='grep --color=auto' vdir='vdir --color=auto'

# Archive extraction helper
ex() {
  if [ -z "$1" ]; then
    echo "Usage: ex <archive>"
  else
    local file="$1"
    if [ -f "$file" ]; then
      mkdir -p "${file%.*}"
      case "$file" in
        *.tar.bz2)   tar xvjf "$file" -C "${file%.*}" ;;
        *.tar.gz)    tar xvzf "$file" -C "${file%.*}" ;;
        *.tar.xz)    tar xvJf "$file" -C "${file%.*}" ;;
        *.lzma)      unlzma "$file"                  ;;
        *.bz2)       bunzip2 "$file"                 ;;
        *.rar)       unrar x -ad "$file" "${file%.*}" ;;
        *.gz)        gunzip "$file"                  ;;
        *.tar)       tar xvf "$file" -C "${file%.*}"  ;;
        *.tbz2)      tar xvjf "$file" -C "${file%.*}" ;;
        *.tgz)       tar xvzf "$file" -C "${file%.*}" ;;
        *.zip)       unzip "$file" -d "${file%.*}"    ;;
        *.Z)         uncompress "$file"              ;;
        *.7z)        7z x "$file" -o"${file%.*}"      ;;
        *.xz)        unxz "$file"                    ;;
        *.exe)       cabextract "$file" -d "${file%.*}" ;;
        *.pkg.tar.zst) tar --use-compress-program=unzstd -xvf "$file" -C "${file%.*}" ;;
        *.pkg.tar.gz) tar xvzf "$file" -C "${file%.*}" ;;
        *)           echo "extract: '$file' - unknown archive method" ;;
      esac
    else
      echo "$file - file does not exist"
    fi
  fi
}

# Initialize tools (lazy load)
initialize_tool() {
  local tool_path="$1" init_command="$2"
  command -v "$tool_path" &>/dev/null && source <("$tool_path" $init_command)
}

# Source bash-preexec if available
[[ -f /usr/share/bash-preexec/bash-preexec.sh ]] && source /usr/share/bash-preexec/bash-preexec.sh

# Initialize tools
initialize_tool "/usr/bin/starship" "init bash --print-full-init"
initialize_tool "/usr/bin/atuin" "init bash"
